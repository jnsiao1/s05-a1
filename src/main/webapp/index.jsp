<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.time.*,java.time.format.DateTimeFormatter,java.util.Date"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Activity-A</title>
</head>
<body>
	
	<%! LocalDateTime now = LocalDateTime.now();
     	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(" yyyy-MM-dd HH:mm:ss");
     	
   
     	LocalDateTime japan = now.plusHours(1);
     	LocalDateTime germany = now.minusHours(7);
     	
    
     	String japanDateTime = japan.format(formatter);
     	String germanyDateTime = germany.format(formatter);
     	
 %>
  	
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila:<%=  now.format(formatter) %> </li>
		<li> Japan: <%=  japanDateTime%></li>
		<li> Germany: <%= germanyDateTime%></li>
	</ul>

	
	<%!
		private int initVar = 1;
		private int serviceVar = 1;
		private int destroyVar = 3;
	%>
	
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
		}
	
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): init"+destroyVar);
		}
	%>
	
	<%
			serviceVar++;
			System.out.println("jspService(): service"+serviceVar);
			String content1 = "content1: " + initVar;
			String content2 = "content2: " + serviceVar;
			String content3 = "content3:" + destroyVar;
	%>
	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
</body>
</html>